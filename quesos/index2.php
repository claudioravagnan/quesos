<?php
session_start();
if(!isset($_SESSION['rol'])){
 //sesion no iniciada
  header("Location:index.html"); 

}
else{
    //sesion iniada correctamente
    $rol = $_SESSION['rol'];
    $nombre = $_SESSION['nombre'];   





}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
    <!--cabecera-->
    <div class="container-fluid">
        <div class="full-whidth-image">
            <img src="img/granja.png" class="img-fluid" style="width: 100%; height: 300px;">
        </div>
    </div>
    </div>

    <!-- titulo cabecera -->
    <div class="row">
        <div class = "col-3"> </div>
        <div class = "col-6"> 
            <div class="alert alert-warning" role="alert"  style="margin-top:  10px;">
                    <center>
                        <h3> BIENVENIDO <strong>  <?php echo $nombre; ?> </strong> al sistema de Trazabilidad  </strong> </h3>
                    </center>
            </div>
        </div>
    </div>        
    <!--fin cabecera-->
    
    <!-- barra menu-->
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="index2.php">Inicio</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item disabled" >
                            <li class="nav-item dropdown " >
                              <?php
                              if($rol != 8) {
                                echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                              }     
                              else{
                                echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                              }
                              ?>
                                Tablas
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" >
                                    <li><a class="dropdown-item" href="#">Usuarios</a></li>
                                    <li><a class="dropdown-item" href="#">Productos</a></li>
                                    <li><a class="dropdown-item" href="#">Cisterna</a></li>
                                    <li><a class="dropdown-item" href="#">Tambos</a></li>
                                    <li><a class="dropdown-item" href="#">Tipo de Fermentos</a></li>
                                </ul>
                                <li class="nav-item dropdown">
                                        <?php
                                    if($rol == 8 || $rol == 6) {
                                        echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                    }     
                                    else{
                                        echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                    }
                                    ?>
                                    Recepcion
                        </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <li><a class="dropdown-item" href="materia_prima.php">Ingreso de Materia Prima</a></li>
                                    </ul>
                                    <li class="nav-item dropdown">
                                    <?php
                                    if($rol == 8 || $rol == 7) {
                                        echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                    }     
                                    else{
                                        echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                    }
                                    ?>
                                    Laboratorio
                          </a>
                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <li><a class="dropdown-item" href="abmlaboratorio.php">Produccion de Fermentos</a></li>
                                        </ul>
                                        <li class="nav-item dropdown">
                                        <?php
                                        if($rol == 8 || $rol == 1) {
                                            echo '<a class="nav-link dropdown-toggle" href="abmlaboratorio.php" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                        }     
                                        else{
                                            echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                        }
                                        ?>
                                        Produccion
                              </a>
                                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <li><a class="dropdown-item" href="creacion_lote.php">Crear Lote de Produccion</a></li>
                                                <li><a class="dropdown-item" href="inicio_produccion1.php">Iniciar Produccíon (Lote)</a></li>
                                                <li><a class="dropdown-item" href="terminar_produccion1.php">Terminar Produccion(Lote)</a></li>
                                            </ul>
                                            <li class="nav-item dropdown">
                                            <?php
                                            if($rol == 8 || $rol == 2) {
                                                echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                            }     
                                            else{
                                                echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                            }
                                            ?>
                                            Saladero
                                  </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                    <li><a class="dropdown-item" href="terminar_saladero1.php">Terminal Lote de Saladero</a></li>
                                                </ul>
                                                <li class="nav-item dropdown">
                                                <?php
                                                if($rol == 8 || $rol == 3) {
                                                    echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                                }     
                                                else{
                                                    echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                                }
                                                ?>    
                                                Curado
                                      </a>
                                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                        <li><a class="dropdown-item" href="#">Terminal Lote de Curado</a></li>
                                                    </ul>

                                                    <li class="nav-item active">
                                                        <a class="nav-link" href="trazabilidad.php">Trazabilidad Lote Producción</span></a>
                                                    </li>

                                                    
                        </ul>
                
              </div>
              <a class="btn btn-warning" href="cerrar.php" role="button">Cerrar Sesión</a>
            </div>
          </nav>
       
        </div>
    </div>
     <!-- Fin barra menu-->



    

    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="img/lacteo 1.jpg" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="img/218825.jpg" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="img/LECHE-industria.gif" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="img/Vacas-lecheras.png" class="d-block w-100" alt="...">
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>





    <footer class="bg-light text-center text-lg-start">
        <!-- Copyright -->
        <div class="text-center p-3 container-fluid style=" background-color: rgba(0, 0, 0, 0.2); ">
        © 2021 Copyright: realizado por 7mo - informatica E.E.S.T. N°1 "Don Luis Bussalleu - Rojas (B) "
        
      </div>
      <!-- Copyright -->
    </footer>




    
</div>




    <script src="js/pooper.min.js "></script>
    <script src="js/bootstrap.min.js "></script>
</body>

</html>




