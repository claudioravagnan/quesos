<?php


$op = array("Crea Lote", "Inicia Producción", "Fin Produccion", "Inicio Saladero", "Fin Saladero", "Inicio Curado", "Fin Curado", "Despacho");  
session_start();
if(!isset($_SESSION['rol'])){
  
  //sesion no iniciada
  header("Location:index.html"); 

}
else{
    //sesion iniada correctamente
    //verifico si es recepcion
    
    $rol = $_SESSION['rol'];
    $nivel = $_SESSION['nivel'];
   
    if($nivel < 6){
      //solo pueden dar ingreso usuario con nivel mayor a 6
      session_destroy();
      header("Location:index.html"); 
    }
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<!--cabecera-->
<div class="container-fluid">
        <div class="full-whidth-image">
            <img src="img/granja.png" class="img-fluid" style="width: 100%; height: 300px;">
        </div>
    </div>
    </div>

    <!-- titulo cabecera -->
    <div class="row">
        <div class = "col-3"> </div>
        <div class = "col-6"> 
            <div class="alert alert-warning" role="alert"  style="margin-top:  10px;">
                    <center>
                        <h3> <strong> Trazabilidad  </strong> </h3>
                    </center>
            </div>
        </div>
    </div>        
    <!--fin cabecera-->


   <!-- barra menu-->
   <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="index2.php">Inicio</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item disabled" >
                            <li class="nav-item dropdown " >
                              <?php
                              if($rol != 8) {
                                echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                              }     
                              else{
                                echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                              }
                              ?>
                                Tablas
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" >
                                    <li><a class="dropdown-item" href="#">Usuarios</a></li>
                                    <li><a class="dropdown-item" href="#">Productos</a></li>
                                    <li><a class="dropdown-item" href="#">Cisterna</a></li>
                                    <li><a class="dropdown-item" href="#">Tambos</a></li>
                                    <li><a class="dropdown-item" href="#">Tipo de Fermentos</a></li>
                                </ul>
                                <li class="nav-item dropdown">
                                        <?php
                                    if($rol == 8 || $rol == 6) {
                                        echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                    }     
                                    else{
                                        echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                    }
                                    ?>
                                    Recepcion
                        </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <li><a class="dropdown-item" href="materia_prima.php">Ingreso de Materia Prima</a></li>
                                    </ul>
                                    <li class="nav-item dropdown">
                                    <?php
                                    if($rol == 8 || $rol == 7) {
                                        echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                    }     
                                    else{
                                        echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                    }
                                    ?>
                                    Laboratorio
                          </a>
                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <li><a class="dropdown-item" href="abmlaboratorio.php">Produccion de Fermentos</a></li>
                                        </ul>
                                        <li class="nav-item dropdown">
                                        <?php
                                        if($rol == 8 || $rol == 1) {
                                            echo '<a class="nav-link dropdown-toggle" href="abmlaboratorio.php" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                        }     
                                        else{
                                            echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                        }
                                        ?>
                                        Produccion
                              </a>
                                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <li><a class="dropdown-item" href="creacion_lote.php">Crear Lote de Produccion</a></li>
                                                <li><a class="dropdown-item" href="inicio_produccion1.php">Ignreso Proceso Produccion(Lote)</a></li>
                                                <li><a class="dropdown-item" href="terminar_produccion1.php">Terminar Proceso Produccion(Lote)</a></li>
                                            </ul>
                                            <li class="nav-item dropdown">
                                            <?php
                                            if($rol == 8 || $rol == 2) {
                                                echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                            }     
                                            else{
                                                echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                            }
                                            ?>
                                            Saladero
                                  </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                    <li><a class="dropdown-item" href="terminar_saladero1.php">Terminal Lote de Saladero</a></li>
                                                </ul>
                                                <li class="nav-item dropdown">
                                                <?php
                                                if($rol == 8 || $rol == 3) {
                                                    echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                                }     
                                                else{
                                                    echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                                }
                                                ?>    
                                                Curado
                                      </a>
                                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                        <li><a class="dropdown-item" href="#">Terminal Lote de Curado</a></li>
                                                    </ul>
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link" href="trazabilidad.php" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Tranzabilidad de Lote
                  </li>
                </ul>
                
              </div>
              <a class="btn btn-warning" href="cerrar.php" role="button">Cerrar Sesión</a>
            </div>
          </nav>
       
        </div>
    </div>
     <!-- Fin barra menu-->
  

     <div>
        
        <form method="POST" action="trazabilidad.php">
        <div class="mb-3 container">
            <label> Lote:</label><br>
            <select name="operaciones" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">                                  
        
                        <?php
                
                       
                            require 'conexion.php'; 
                            $query = 'SELECT * FROM produccion';
                            $result = mysqli_query($con, $query);
                            while ($valores = mysqli_fetch_array ($result)) {
                                echo '<option value = '.$valores['id_produccion'].'>'.$valores['id_produccion'].'</option>';
                            
                            }
                           
                ?>
                </select>
            
            <input type="submit" value = "Consultar" name="boton"> 

            

        </form>
    

</div>

<div class="container">
<?php 
            if(isset($_POST['boton']))
            {
                $lote = $_POST['operaciones'];

                //creamos creacion de lote
                require 'conexion.php'; 
                $query = 'SELECT * FROM produccion, productos, fermentos where id_produccion='.$lote .' and produccion.id_producto = productos.id_producto and produccion.id_fermento = fermentos.id_fermento';
                $result = mysqli_query($con, $query);
                    echo "<br>";
                    echo "<strong>Lote/Producto </strong>";
                    echo ('<table class="table table-striped bg-info">');
                    echo '<tr>';
                    echo '<th> Lote </th>';
                    echo '<th> Producto </th>';
                    echo '<th> Cant. leche </th>';
                    echo '<th> Fermento </th>';
                    echo '<th> Cisterna </th>';
                    echo'  </tr>';
                if(mysqli_num_rows($result)>0){
                    $valores = mysqli_fetch_array ($result);
                    echo '<tr>';
                    echo "<td>" .$valores['id_produccion']."</td>";
                    echo "<td>" .$valores['producto']."</td>";
                    echo "<td>" .$valores['litros_entrada']."</td>";
                     echo "<td>" .$valores['fermento']."</td>";
                    echo "<td>" .$valores['id_cisterna']."</td>";
                    echo '</tr>';   
                   }
                   echo "</table>";
                
                   echo "<br>";


                   echo "<br>";
                   echo "<strong>Operaciones </strong>";
                     
                   
                    echo ('<table class="table table-striped bg-danger">');
                    echo '<tr>';
                    echo '<th>Lote</th>';
                    echo '<th> Fecha Entrada </th>';
                    echo '<th> Hora Entrada </th>';
                      echo '<th> Kgms Entrada </th>';
                     echo '<th> Fecha Salida </th>';
                    echo '<th> Hora Salida </th>';
                   echo '<th> Kgms Salida </th>';
                    echo '<th> Unidades </th>';
                    echo '<th> Operación</th>';
                   
                    echo'  </tr>';


                    //creamos creacion de lote
                   require 'conexion.php'; 
                   $query = 'SELECT * FROM produccion where id_produccion='.$lote;
                   $result = mysqli_query($con, $query);
                   

                    
                   require 'conexion.php'; 
                   $query = 'SELECT * FROM operaciones where id_produccion='.$lote;
                   $result = mysqli_query($con, $query);
                   while ($valores = mysqli_fetch_array ($result)) {
                    echo '<tr>';
                    echo "<td>" .$valores['id_produccion']."</td>";
                   
                    if($valores['fecha_entrada']== "0000-00-00"){
                      echo "<td> </td>";
                    }
                    else{
                      echo "<td>" .$valores['fecha_entrada']."</td>";
                    }
                    if($valores['hora_entrada']== "00:00:00"){
                      echo "<td> </td>";
                    }
                    else{
                      echo "<td>" .$valores['hora_entrada']."</td>";
                    }
                    if($valores['peso_entrada']== 0){
                      echo "<td> </td>";
                    }
                    else{
                      echo "<td>" .$valores['peso_entrada']."</td>";
                    }
                    
                    if($valores['fecha_salida']== "0000-00-00"){
                      echo "<td> </td>";
                    }
                    else{
                      echo "<td>" .$valores['fecha_salida']."</td>";
                    }
                    if($valores['hora_salida']== "00:00:00"){
                      echo "<td> </td>";
                    }
                    else{
                      echo "<td>" .$valores['hora_salida']."</td>";
                    }
                    if($valores['peso_salida']== 0){
                      echo "<td> </td>";
                    }
                    else{
                      echo "<td>" .$valores['peso_salida']."</td>";
                    }
                    
                    
                    echo "<td>" .$valores['unidades']."</td>";
                    echo "<td>" . $op[$valores['tipo_op']]."</td>";
                    
                    echo '</tr>';   
                   }
                   echo "</table>";
                }
?>

</div>


    




        <div>

        <footer class="bg-light text-center text-lg-start">
        
            <div class="text-center p-3 container-fluid style=" background-color: rgba(0, 0, 0, 0.2);>
            © 2021 Copyright: realizado por 7mo - informatica E.E.S.T. N°1 "Don Luis Bussalleu - Rojas (B) "
            
        </div>
        </footer>
    


    <script src="js/pooper.min.js "></script>
    <script src="js/bootstrap.min.js "></script>
</body>

</html>