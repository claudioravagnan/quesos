<?php
session_start();
if(!isset($_SESSION['rol'])){
  
  //sesion no iniciada
  header("Location:index.html"); 

}
else{
    //sesion iniada correctamente
    //verifico si es recepcion
    
    $rol = $_SESSION['rol'];
    $nivel = $_SESSION['nivel'];
   
    if($nivel < 6){
      //solo pueden dar ingreso usuario con nivel mayor a 6
      session_destroy();
      header("Location:index.html"); 
    }
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<!--cabecera-->
<div class="container-fluid">
        <div class="full-whidth-image">
            <img src="img/granja.png" class="img-fluid" style="width: 100%; height: 300px;">
        </div>
    </div>
    </div>

    <!-- titulo cabecera -->
    <div class="row">
        <div class = "col-3"> </div>
        <div class = "col-6"> 
            <div class="alert alert-warning" role="alert"  style="margin-top:  10px;">
                    <center>
                        <h3> <strong> Ingreso Materia Prima  </strong> </h3>
                    </center>
            </div>
        </div>
    </div>        
    <!--fin cabecera-->


   <!-- barra menu-->
   <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="index2.php">Inicio</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item disabled" >
                            <li class="nav-item dropdown " >
                              <?php
                              if($rol != 8) {
                                echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                              }     
                              else{
                                echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                              }
                              ?>
                                Tablas
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" >
                                    <li><a class="dropdown-item" href="#">Usuarios</a></li>
                                    <li><a class="dropdown-item" href="#">Productos</a></li>
                                    <li><a class="dropdown-item" href="#">Cisterna</a></li>
                                    <li><a class="dropdown-item" href="#">Tambos</a></li>
                                    <li><a class="dropdown-item" href="#">Tipo de Fermentos</a></li>
                                </ul>
                                <li class="nav-item dropdown">
                                        <?php
                                    if($rol == 8 || $rol == 6) {
                                        echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                    }     
                                    else{
                                        echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                    }
                                    ?>
                                    Recepcion
                        </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <li><a class="dropdown-item" href="materia_prima.php">Ingreso de Materia Prima</a></li>
                                    </ul>
                                    <li class="nav-item dropdown">
                                    <?php
                                    if($rol == 8 || $rol == 7) {
                                        echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                    }     
                                    else{
                                        echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                    }
                                    ?>
                                    Laboratorio
                          </a>
                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <li><a class="dropdown-item" href="abmlaboratorio.php">Produccion de Fermentos</a></li>
                                        </ul>
                                        <li class="nav-item dropdown">
                                        <?php
                                        if($rol == 8 || $rol == 1) {
                                            echo '<a class="nav-link dropdown-toggle" href="abmlaboratorio.php" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                        }     
                                        else{
                                            echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                        }
                                        ?>
                                        Produccion
                              </a>
                                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <li><a class="dropdown-item" href="creacion_lote.php">Crear Lote de Produccion</a></li>
                                                <li><a class="dropdown-item" href="inicio_produccion1.php">Ignreso Proceso Produccion(Lote)</a></li>
                                                <li><a class="dropdown-item" href="terminar_produccion1.php">Terminar Proceso Produccion(Lote)</a></li>
                                            </ul>
                                            <li class="nav-item dropdown">
                                            <?php
                                            if($rol == 8 || $rol == 2) {
                                                echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                            }     
                                            else{
                                                echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                            }
                                            ?>
                                            Saladero
                                  </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                    <li><a class="dropdown-item" href="terminar_saladero1.php">Terminal Lote de Saladero</a></li>
                                                </ul>
                                                <li class="nav-item dropdown">
                                                <?php
                                                if($rol == 8 || $rol == 3) {
                                                    echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                                }     
                                                else{
                                                    echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                                }
                                                ?>    
                                                Curado
                                      </a>
                                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                        <li><a class="dropdown-item" href="#">Terminal Lote de Curado</a></li>
                                                    </ul>
                                                    <li class="nav-item active">
                                                        <a class="nav-link" href="trazabilidad.php">Trazabilidad Lote Producción</span></a>
                                                    </li>
                </ul>
                
              </div>
              <a class="btn btn-warning" href="cerrar.php" role="button">Cerrar Sesión</a>
            </div>
          </nav>
       
        </div>
    </div>
     <!-- Fin barra menu-->
  
    

   
    <!-- formulario -->
    <form method="GET" action="abm_ingreso.php" enctype="multipart/form-data">
            <div class="mb-3 container"> 
                <label for="" class="form-label">Fecha</label>
                <input type="date" name="fecha" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">Ingrese la fecha de ingreso </div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Hora</label>
                <input type="time" name="hora" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese la hora de ingreso</div>
            </div>

            
            <div class="mb-3 container">
            <label> Tambo:</label><br>
            <select name="tambo" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
            <?php
           require 'conexion.php'; 
           $query = 'SELECT * FROM tambos';
           $result = mysqli_query($con, $query);
           while ($valores = mysqli_fetch_array ($result)) {
               echo '<option value = '.$valores['id_tambo'].'>'.$valores['empresa'].'</option>';
           }
        
            ?>   </select><br>
            </div>
            
            <div class="mb-3 container">
                <label for="" class="form-label">Patente del camion</label>
                <input type="text" name="patente_camion" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">ingrese la patente de el vehiculo</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">DNI del chofer</label>
                <input type="text" name="dni_chofer" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el numero de documento del chofer</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Nombre del chofer</label>
                <input type="text" name="nombre_chofer" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el nombre completo del chofer</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label"> Litros</label>
                <input type="number" name="litros" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese la cantidad de litros en numeros</div>
            </div>
            <div class="mb-3 container">
            <label for="exampleInputEmail1" class="form-label"> Cisterna</label>
            <select name = "cisterna" class="form-select form-select-sm mb-3" aria-label=".form-select-lg example">
            
            <?php

           require 'conexion.php'; 
           $query = 'SELECT * FROM cisternas';
           $result = mysqli_query($con, $query);
           while ($valores = mysqli_fetch_array ($result)) {
               echo '<option value = '.$valores['id_cisterna'].'>'.$valores['numero_cisterna'].'</option>';
           }
            ?></select> <br>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label"> Calidad</label>
                <input type="text" name="calidad" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el estado de calidad</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label"> temperatura</label>
                <input type="number" name="temperatura" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese la temperatura</div>
            </div>

          

            <div class="mb-3 container">
            <input type="submit" value="Registrar"><br><br>
          </div>

      </form>

          
    </div>
    
</body>

</html>




    <footer class="bg-light text-center text-lg-start">
        
        <div class="text-center p-3 container-fluid style=" background-color: rgba(0, 0, 0, 0.2); ">
        © 2021 Copyright: realizado por 7mo - informatica E.E.S.T. N°1 "Don Luis Bussalleu - Rojas (B) "
        
      </div>
    
    </footer>




    
</div>




   
</body>
<script src="js/pooper.min.js "></script>
    <script src="js/bootstrap.min.js "></script>
</html>
