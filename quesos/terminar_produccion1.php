<?php
session_start();
if(!isset($_SESSION['rol'])){
  
  //sesion no iniciada
  header("Location:index.html"); 

}
else{
    //sesion iniada correctamente
    //verifico si es recepcion
    
    $rol = $_SESSION['rol'];
    $nivel = $_SESSION['nivel'];
   
    if($nivel < 6){
      //solo pueden dar ingreso usuario con nivel mayor a 6
      session_destroy();
      header("Location:index.html"); 
    }
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<!--cabecera-->
<div class="container-fluid">
        <div class="full-whidth-image">
            <img src="img/granja.png" class="img-fluid" style="width: 100%; height: 300px;">
        </div>
    </div>
    </div>

    <!-- titulo cabecera -->
    <div class="row">
        <div class = "col-3"> </div>
        <div class = "col-6"> 
            <div class="alert alert-warning" role="alert"  style="margin-top:  10px;">
                    <center>
                        <h3> <strong> Terminar producción por lote  </strong> </h3>
                    </center>
            </div>
        </div>
    </div>        
    <!--fin cabecera-->


   <!-- barra menu-->
   <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="index2.php">Inicio</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item disabled" >
                            <li class="nav-item dropdown " >
                              <?php
                              if($rol != 8) {
                                echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                              }     
                              else{
                                echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                              }
                              ?>
                                Tablas
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" >
                                    <li><a class="dropdown-item" href="#">Usuarios</a></li>
                                    <li><a class="dropdown-item" href="#">Productos</a></li>
                                    <li><a class="dropdown-item" href="#">Cisterna</a></li>
                                    <li><a class="dropdown-item" href="#">Tambos</a></li>
                                    <li><a class="dropdown-item" href="#">Tipo de Fermentos</a></li>
                                </ul>
                                <li class="nav-item dropdown">
                                        <?php
                                    if($rol == 8 || $rol == 6) {
                                        echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                    }     
                                    else{
                                        echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                    }
                                    ?>
                                    Recepcion
                        </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <li><a class="dropdown-item" href="materia_prima.php">Ingreso de Materia Prima</a></li>
                                    </ul>
                                    <li class="nav-item dropdown">
                                    <?php
                                    if($rol == 8 || $rol == 7) {
                                        echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                    }     
                                    else{
                                        echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                    }
                                    ?>
                                    Laboratorio
                          </a>
                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <li><a class="dropdown-item" href="abmlaboratorio.php">Produccion de Fermentos</a></li>
                                        </ul>
                                        <li class="nav-item dropdown">
                                        <?php
                                        if($rol == 8 || $rol == 1) {
                                            echo '<a class="nav-link dropdown-toggle" href="abmlaboratorio.php" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                        }     
                                        else{
                                            echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                        }
                                        ?>
                                        Produccion
                              </a>
                                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <li><a class="dropdown-item" href="creacion_lote.php">Crear Lote de Produccion</a></li>
                                                <li><a class="dropdown-item" href="inicio_produccion1.php">Ignreso Proceso Produccion(Lote)</a></li>
                                                <li><a class="dropdown-item" href="terminar_produccion1.php">Terminar Proceso Produccion(Lote)</a></li>
                                            </ul>
                                            <li class="nav-item dropdown">
                                            <?php
                                            if($rol == 8 || $rol == 2) {
                                                echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                            }     
                                            else{
                                                echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                            }
                                            ?>
                                            Saladero
                                  </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                    <li><a class="dropdown-item" href="terminar_saladero1.php">Terminal Lote de Saladero</a></li>
                                                </ul>
                                                <li class="nav-item dropdown">
                                                <?php
                                                if($rol == 8 || $rol == 3) {
                                                    echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';      
                                                }     
                                                else{
                                                    echo '<a class="nav-link dropdown-toggle disabled" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
                                                }
                                                ?>    
                                                Curado
                                      </a>
                                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                        <li><a class="dropdown-item" href="#">Terminal Lote de Curado</a></li>
                                                    </ul>
                                                    <li class="nav-item active">
                                                        <a class="nav-link" href="trazabilidad.php">Trazabilidad Lote Producción</span></a>
                                                    </li>
                </ul>
                
              </div>
              <a class="btn btn-warning" href="cerrar.php" role="button">Cerrar Sesión</a>
            </div>
          </nav>
       
        </div>
    </div>
     <!-- Fin barra menu-->

<!--Formulario -->
    <div>
        <form method="GET" action="terminar_produccion2.php">
        <div class="mb-3 container">
                <label for="" class="form-label">Lote</label>
                <select name="lote" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
           
               <?php
                  require 'conexion.php'; 
                  $query = 'SELECT* FROM produccion WHERE etapa=1';
                  $result = mysqli_query($con, $query);
                  while ($valores = mysqli_fetch_array ($result)) {
                      echo '<option value = '.$valores['id_produccion'].'>Lote '.$valores['id_produccion'].'</option>';
                  }
            ?>
            </select>
            <div id="emailHelp" class="form-text">Ingrese el lote de produccion </div>
            
             <br>
        </div>
            
            
            <div class="mb-3 container">
                <label for="" class="form-label">Fecha de Salida</label>
                <input type="date" name="fechasalida" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">Ingrese la fecha de salida</div>
            </div>
            <div class="mb-3 container">
                <label for="" class="form-label">Hora de Salida</label>
                <input type="time" name="horasalida" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">Ingrese la fecha de entrada </div>
            </div>
            <div class="mb-3 container">
                <label for="" class="form-label">Kgms Salida</label>
                <input type="number" name="pesosalida" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">Ingrese la fecha de entrada </div>
            </div>
            

<input type="submit" value="Grabar"><br><br>
        </form>

    </div>
</body>
<script src="js/pooper.min.js "></script>
    <script src="js/bootstrap.min.js "></script>
</html>