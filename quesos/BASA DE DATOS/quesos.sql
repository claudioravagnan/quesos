-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-11-2021 a las 04:59:54
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `quesos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cisternas`
--

CREATE TABLE `cisternas` (
  `id_cisterna` int(11) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `numero_cisterna` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `cisternas`
--

INSERT INTO `cisternas` (`id_cisterna`, `capacidad`, `numero_cisterna`) VALUES
(1, 1000, 1),
(2, 1000, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id_departamentos` int(11) NOT NULL,
  `departamento` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id_departamentos`, `departamento`) VALUES
(1, 'produccion'),
(2, 'saladero'),
(3, 'curado'),
(4, 'despacho'),
(5, 'administracion'),
(6, 'Recepcion'),
(7, 'Laboratorio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fermentos`
--

CREATE TABLE `fermentos` (
  `id_fermento` int(11) NOT NULL,
  `fermento` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `fermentos`
--

INSERT INTO `fermentos` (`id_fermento`, `fermento`) VALUES
(1, 'bacteria rn72'),
(2, 'bacteria dr34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingreso_materia_prima`
--

CREATE TABLE `ingreso_materia_prima` (
  `id_ingreso` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `litros` int(11) NOT NULL,
  `calidad` varchar(100) COLLATE utf8_bin NOT NULL,
  `temperatura` int(11) NOT NULL,
  `patente_camion` varchar(20) COLLATE utf8_bin NOT NULL,
  `dni_chofer` varchar(20) COLLATE utf8_bin NOT NULL,
  `nombre_chofer` text COLLATE utf8_bin NOT NULL,
  `id_tambo` int(11) NOT NULL,
  `id_cisterna` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `ingreso_materia_prima`
--

INSERT INTO `ingreso_materia_prima` (`id_ingreso`, `fecha`, `hora`, `litros`, `calidad`, `temperatura`, `patente_camion`, `dni_chofer`, `nombre_chofer`, `id_tambo`, `id_cisterna`) VALUES
(5, '2021-11-26', '15:05:00', 1250, 'Optima', 21, 'BRX 123', '21345657', 'Camilo', 1, 1),
(6, '2021-11-27', '17:28:00', 1300, 'Optima', 31, 'rrs 234', '41356789', 'Sandro', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratorio`
--

CREATE TABLE `laboratorio` (
  `id_laboratorio` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `peso` double NOT NULL,
  `numero_frasco` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `id_fermento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `laboratorio`
--

INSERT INTO `laboratorio` (`id_laboratorio`, `fecha`, `hora`, `peso`, `numero_frasco`, `cantidad`, `id_fermento`) VALUES
(1, '2021-11-25', '10:37:00', 0, 213, 12, 2),
(2, '2021-11-25', '19:30:00', 0, 324, 12, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operaciones`
--

CREATE TABLE `operaciones` (
  `id_operacion` int(11) NOT NULL,
  `fecha_entrada` date NOT NULL,
  `fecha_salida` date NOT NULL,
  `hora_entrada` time NOT NULL,
  `hora_salida` time NOT NULL,
  `peso_entrada` double NOT NULL,
  `peso_salida` double NOT NULL,
  `unidades` int(11) NOT NULL,
  `id_departamento` int(11) NOT NULL,
  `id_produccion` int(11) NOT NULL,
  `tipo_op` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `operaciones`
--

INSERT INTO `operaciones` (`id_operacion`, `fecha_entrada`, `fecha_salida`, `hora_entrada`, `hora_salida`, `peso_entrada`, `peso_salida`, `unidades`, `id_departamento`, `id_produccion`, `tipo_op`) VALUES
(21, '2021-11-26', '0000-00-00', '18:20:00', '00:00:00', 890, 0, 0, 1, 7, 1),
(22, '0000-00-00', '2021-11-27', '00:00:00', '05:33:00', 0, 789, 0, 1, 7, 2),
(23, '0000-00-00', '2021-11-27', '00:00:00', '21:09:00', 0, 756, 0, 3, 7, 4),
(24, '2021-11-27', '0000-00-00', '18:33:00', '00:00:00', 970, 0, 0, 1, 8, 1),
(25, '0000-00-00', '2021-11-29', '00:00:00', '19:34:00', 0, 950, 0, 1, 8, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `produccion`
--

CREATE TABLE `produccion` (
  `id_produccion` int(11) NOT NULL,
  `litros_entrada` int(11) NOT NULL,
  `id_cisterna` int(11) NOT NULL,
  `id_fermento` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `etapa` int(11) NOT NULL,
  `frasco` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `produccion`
--

INSERT INTO `produccion` (`id_produccion`, `litros_entrada`, `id_cisterna`, `id_fermento`, `id_producto`, `etapa`, `frasco`) VALUES
(7, 1000, 1, 1, 2, 4, 213),
(8, 1000, 2, 2, 8, 3, 0),
(9, 1000, 1, 1, 9, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `producto` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `producto`) VALUES
(1, 'cheddar'),
(2, 'brie'),
(3, 'azul'),
(4, 'roquefor'),
(5, 'provolone'),
(6, 'camembert'),
(7, 'chaumes'),
(8, 'palmesano '),
(9, 'mozzarella'),
(10, 'gouda');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tambos`
--

CREATE TABLE `tambos` (
  `id_tambo` int(11) NOT NULL,
  `empresa` varchar(100) COLLATE utf8_bin NOT NULL,
  `direccion` varchar(50) COLLATE utf8_bin NOT NULL,
  `telefono` varchar(50) COLLATE utf8_bin NOT NULL,
  `e-mail` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `tambos`
--

INSERT INTO `tambos` (`id_tambo`, `empresa`, `direccion`, `telefono`, `e-mail`) VALUES
(1, 'la vaca loca', 'roberto cano ', '54632567', 'info@vacaloca.com'),
(2, 'don miguel', 'castelli 89', '34567890', 'info@donmiguel.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(50) COLLATE utf8_bin NOT NULL,
  `contraseña` varchar(255) COLLATE utf8_bin NOT NULL,
  `nivel` int(11) NOT NULL,
  `id_departamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `contraseña`, `nivel`, `id_departamento`) VALUES
(1, 'administrador', 'b59c67bf196a4758191e42f76670ceba', 9, 8),
(2, 'recepcion', 'b59c67bf196a4758191e42f76670ceba', 9, 6),
(3, 'laboratorio', 'b59c67bf196a4758191e42f76670ceba', 9, 7),
(4, 'produccion', 'b59c67bf196a4758191e42f76670ceba', 9, 1),
(5, 'saladero', 'b59c67bf196a4758191e42f76670ceba', 9, 2),
(6, 'curado', 'b59c67bf196a4758191e42f76670ceba', 9, 3),
(7, 'despacho', 'b59c67bf196a4758191e42f76670ceba', 9, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cisternas`
--
ALTER TABLE `cisternas`
  ADD PRIMARY KEY (`id_cisterna`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id_departamentos`);

--
-- Indices de la tabla `fermentos`
--
ALTER TABLE `fermentos`
  ADD PRIMARY KEY (`id_fermento`);

--
-- Indices de la tabla `ingreso_materia_prima`
--
ALTER TABLE `ingreso_materia_prima`
  ADD PRIMARY KEY (`id_ingreso`);

--
-- Indices de la tabla `laboratorio`
--
ALTER TABLE `laboratorio`
  ADD PRIMARY KEY (`id_laboratorio`);

--
-- Indices de la tabla `operaciones`
--
ALTER TABLE `operaciones`
  ADD PRIMARY KEY (`id_operacion`);

--
-- Indices de la tabla `produccion`
--
ALTER TABLE `produccion`
  ADD PRIMARY KEY (`id_produccion`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `tambos`
--
ALTER TABLE `tambos`
  ADD PRIMARY KEY (`id_tambo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cisternas`
--
ALTER TABLE `cisternas`
  MODIFY `id_cisterna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id_departamentos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `fermentos`
--
ALTER TABLE `fermentos`
  MODIFY `id_fermento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ingreso_materia_prima`
--
ALTER TABLE `ingreso_materia_prima`
  MODIFY `id_ingreso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `laboratorio`
--
ALTER TABLE `laboratorio`
  MODIFY `id_laboratorio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `operaciones`
--
ALTER TABLE `operaciones`
  MODIFY `id_operacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `produccion`
--
ALTER TABLE `produccion`
  MODIFY `id_produccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tambos`
--
ALTER TABLE `tambos`
  MODIFY `id_tambo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
