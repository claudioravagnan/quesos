<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <title>Inicio de produccion</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<div class="container-fluid">
        <div class="full-whidth-image">
            <img src="img/granja.png" class="img-fluid" style="width: 1500px; height: 300px;">
        </div>
    </div>
    </div>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
              <a class="navbar-brand" href="#">Inicio</a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Tablas
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <li><a class="dropdown-item" href="#">Usuarios</a></li>
                          <li><a class="dropdown-item" href="#">Productos</a></li>
                          <li><a class="dropdown-item" href="#">Cisterna</a></li>
                          <li><a class="dropdown-item" href="#">Tambos</a></li>
                          <li><a class="dropdown-item" href="#">Tipo de Fermentos</a></li>
                        </ul>
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Recepcion
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <li><a class="dropdown-item" href="materia_prima.php">Ingreso de Materia Prima</a></li>
                        </ul>
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Laboratorio
                          </a>
                          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="#">Produccion de Fermentos</a></li>
                          </ul> 
                          <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Produccion
                              </a>
                              <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="#">Inicio Lote de Produccion</a></li>
                                <li><a class="dropdown-item" href="#">Terminal Lote de Produccion</a></li>
                              </ul> 
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Saladero
                                  </a>
                                  <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="#">Terminal Lote de Saladero</a></li>
                                  </ul> 
                                  <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Curado
                                      </a>
                                      <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <li><a class="dropdown-item" href="#">Terminal Lote de Curado</a></li>
                                      </ul> 
                                      <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Tranzabilidad de Lote
                  </li>
                </ul>
              </div>
            </div>
          </nav>
       
        </div>
    </div>
    <div>
        <form method="GET" action="inicio_saladero2.php">
        <center>
            <h1>Inicio Saladero</h1>
          </center>
        <div class="mb-3 container">
                <label for="" class="form-label">Lote</label>
                <select name="lote" class="form-control"><br>
            <?php
           require 'conexion.php'; 
           $query = 'SELECT* FROM produccion WHERE etapa=2';
           $result = mysqli_query($con, $query);
           while ($valores = mysqli_fetch_array ($result)) {
               echo '<option value = '.$valores['id_produccion'].'>Lote '.$valores['id_produccion'].'</option>';
           }
            ?></select> <br>
             <div class="mb-3 container">
                <label for="" class="form-label">Fecha de Entrada</label>
                <input type="date" name="fechaentrada" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">Ingrese la fecha de salida</div>
            </div>
            <div class="mb-3 container">
                <label for="" class="form-label">Hora de Entrada</label>
                <input type="time" name="horaentrada" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">Ingrese la fecha de entrada </div>
            </div>
            <div class="mb-3 container">
                <label for="" class="form-label">Peso de Entrada</label>
                <input type="number" name="pesoentrada" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">Ingrese la fecha de entrada </div>
            </div>
            

<input type="submit" value="Grabar"><br><br>
        </form>

    </div>
</body>
<script src="js/pooper.min.js "></script>
    <script src="js/bootstrap.min.js "></script>
</html>