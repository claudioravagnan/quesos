<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
    <div class="container-fluid">
        <div class="full-whidth-image">
            <img src="img/granja.png" class="img-fluid" style="width: 1500px; height: 300px;">
        </div>
    </div>
    </div>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
              <a class="navbar-brand" href="index.html">INICIO</a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Tablas
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <li><a class="dropdown-item" href="#">Usuarios</a></li>
                          <li><a class="dropdown-item" href="#">Productos</a></li>
                          <li><a class="dropdown-item" href="#">Cisterna</a></li>
                          <li><a class="dropdown-item" href="#">Tambos</a></li>
                          <li><a class="dropdown-item" href="#">Tipo de Fermentos</a></li>
                        </ul>
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Recepcion
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <li><a class="dropdown-item" href="materia_prima.php">Ingreso de Materia Prima</a></li>
                        </ul>
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Laboratorio
                          </a>
                          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="#">Produccion de Fermentos</a></li>
                          </ul> 
                          <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Produccion
                              </a>
                              <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="#">Inicio Lote de Produccion</a></li>
                                <li><a class="dropdown-item" href="#">Terminal Lote de Produccion</a></li>
                              </ul> 
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Saladero
                                  </a>
                                  <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="#">Terminal Lote de Saladero</a></li>
                                  </ul> 
                                  <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Curado
                                      </a>
                                      <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <li><a class="dropdown-item" href="#">Terminal Lote de Curado</a></li>
                                      </ul> 
                                      <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Tranzabilidad de Lote</a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        
    </div>
    <!DOCTYPE html>


<html>

<head>
    <meta charset='utf-8'>
    <title>Ingresos de Material</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

</head>

<body>
  
    <div>
    <center>
      <h1>INGRESO DE MATERIA PRIMA</h1>
      </center>
    <form method="GET" action="abm_ingresos2.php" enctype="multipart/form-data">
            <div class="mb-3 container"> 
                <label for="" class="form-label">Fecha</label>
                <input type="date" name="fecha" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">Ingrese la fecha de ingreso </div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Hora</label>
                <input type="time" name="hora" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese la hora de ingreso</div>
            </div>

            
            <div class="mb-3 container">
            <label> Tambo:</label><br>
        <select name ="tambo">
            <?php
           require 'conexion.php'; 
           $query = 'SELECT * FROM tambos';
           $result = mysqli_query($con, $query);
           while ($valores = mysqli_fetch_array ($result)) {
               echo '<option value = '.$valores['id_tambo'].'>'.$valores['empresa'].'</option>';
           }
        
            ?>   </select><br>
            </div>
            
            <div class="mb-3 container">
                <label for="" class="form-label">Patente del camion</label>
                <input type="text" name="patente_camion" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">ingrese la patente de el vehiculo</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">DNI del chofer</label>
                <input type="text" name="dni_chofer" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el numero de documento del chofer</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Nombre del chofer</label>
                <input type="text" name="nombre_chofer" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el nombre completo del chofer</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label"> Litros</label>
                <input type="number" name="litros" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese la cantidad de litros en numeros</div>
            </div>
            <div class="mb-3 container">
            <label for="exampleInputEmail1" class="form-label"> Cisterna</label>
            <select name ="cisterna">
            
            <?php

           require 'conexion.php'; 
           $query = 'SELECT * FROM cisternas';
           $result = mysqli_query($con, $query);
           while ($valores = mysqli_fetch_array ($result)) {
               echo '<option value = '.$valores['id_cisterna'].'>'.$valores['numero_cisterna'].'</option>';
           }
            ?></select> <br>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label"> Calidad</label>
                <input type="text" name="calidad" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el estado de calidad</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label"> temperatura</label>
                <input type="number" name="temperatura" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese la temperatura</div>
            </div>

          

            <div class="mb-3 container">
            <input type="submit" value="Registrar"><br><br>
          </div>

      </form>

          
    </div>
    
</body>

</html>




    <footer class="bg-light text-center text-lg-start">
        
        <div class="text-center p-3 container-fluid style=" background-color: rgba(0, 0, 0, 0.2); ">
        © 2021 Copyright: realizado por 7mo - informatica E.E.S.T. N°1 "Don Luis Bussalleu - Rojas (B) "
        
      </div>
    
    </footer>




    
</div>




   
</body>
<script src="js/pooper.min.js "></script>
    <script src="js/bootstrap.min.js "></script>
</html>
