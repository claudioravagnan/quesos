<?php
session_start();
if(!isset($_SESSION['rol'])){
  
  //sesion no iniciada
  header("Location:index.html"); 

}
else{
    //sesion iniada correctamente
    //verifico si es recepcion
    
    $rol = $_SESSION['rol'];
    $nivel = $_SESSION['nivel'];
   
    if($nivel < 6){
      //solo pueden dar ingreso usuario con nivel mayor a 6
      session_destroy();
      header("Location:index.html"); 
    }
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<!--cabecera-->
<div class="container-fluid">
        <div class="full-whidth-image">
            <img src="img/granja.png" class="img-fluid" style="width: 100%; height: 300px;">
        </div>
    </div>
    </div>

    <!-- titulo cabecera -->
    <div class="row">
        <div class = "col-3"> </div>
        <div class = "col-6"> 
            <div class="alert alert-warning" role="alert"  style="margin-top:  10px;">
                    <center>
                        <h3> <strong> Producción de fermentos  </strong> </h3>
                    </center>
            </div>
        </div>
    </div>        
    <!--fin cabecera-->


    
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
              <a class="navbar-brand" href="index2.php">INICIO</a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Tablas
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <li><a class="dropdown-item" href="#">Usuarios</a></li>
                          <li><a class="dropdown-item" href="#">Productos</a></li>
                          <li><a class="dropdown-item" href="#">Cisterna</a></li>
                          <li><a class="dropdown-item" href="#">Tambos</a></li>
                          <li><a class="dropdown-item" href="#">Tipo de Fermentos</a></li>
                        </ul>
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Recepcion
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <li><a class="dropdown-item" href="materia_prima.php">Ingreso de Materia Prima</a></li>
                        </ul>
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Laboratorio
                          </a>
                          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="#">Produccion de Fermentos</a></li>
                          </ul> 
                          <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Produccion
                              </a>
                              <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="#">Inicio Lote de Produccion</a></li>
                                <li><a class="dropdown-item" href="#">Terminal Lote de Produccion</a></li>
                              </ul> 
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Saladero
                                  </a>
                                  <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="#">Terminal Lote de Saladero</a></li>
                                  </ul> 
                                  <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Curado
                                      </a>
                                      <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <li><a class="dropdown-item" href="#">Terminal Lote de Curado</a></li>
                                      </ul> 
                                      <li class="nav-item active">
                                                        <a class="nav-link" href="trazabilidad.php">Trazabilidad Lote Producción</span></a>
                                                    </li>
                </ul>
              </div>
            </div>
          </nav>
        
    </div>

<!-- Formulario -->

        <form method="GET" action="abmlaboratorio2.php" >
            <div class="mb-3 container">
                <label for="" class="form-label">Fecha</label>
                <input type="date" name="fecha" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="este campo es obligatorio">
                <div id="emailHelp" class="form-text">Ingrese la fecha de ingreso </div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Hora</label>
                <input type="time" name="hora" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese la hora de ingreso</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Numero de frasco</label>
                <input type="number" name="n_frasco" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese el numero de frasco</div>
            </div>
            <div class="mb-3 container">
                <label for="exampleInputEmail1" class="form-label">Cantidad</label><br>
                <input type="number" name="cantidad" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">Ingrese la cantidad de fermeto</div>
            </div>
          

            <div class="mb-3 container">
            <label class="form-label">Fermento</label> <br>
            <select name="id_fermento" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
            <?php
           

        require 'conexion.php';
        $query= 'SELECT * from fermentos';
        $result = mysqli_query($con,$query);
        
        while($valores= mysqli_fetch_array($result)){

            echo'<option value="'.$valores['id_fermento'].'">'.$valores['fermento'].'</option>';

        }
           ?>    
        </select>
    
        <br>
        <br>
    </div>
          <div>  
    <input type="submit" value="Registrar"><br><br>
            </div>
        </form>

    </div>





    <footer class="bg-light text-center text-lg-start">
        <!-- Copyright -->
        <div class="text-center p-3 container-fluid style=" background-color: rgba(0, 0, 0, 0.2); ">
        © 2021 Copyright: realizado por 7mo - informatica E.E.S.T. N°1 "Don Luis Bussalleu - Rojas (B) "
        
      </div>
      <!-- Copyright -->
    </footer>




    
</div>



































    

    <script src="js/pooper.min.js "></script>
    <script src="js/bootstrap.min.js "></script>

</body>

</html>